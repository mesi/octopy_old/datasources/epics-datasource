import time
import json
import pvaccess
from lib.secop import datainfo


class EpicsReader(object):
    """ Object for monitoring updates of a PV.
    Can subscribes to a PV and activate a monitor of that PV.
    A PV update will trigger publishing of the updated data to MQTT by the on_epics-reader_event of the Link_client
    below.
    Arg: get_reference  PV name of existing PV running on external IOC/device
    """
    def __init__(self, get_reference):
        self.pv_name = get_reference
        self._on_message = None
        self.pv_channel = pvaccess.Channel(self.pv_name)
        self.subscribing = False
        self.monitoring = False
        self.data = None
        self.secop_type = None
        self.epics_type = None
        self.unit = None
        self.description = None
        self.retries = 0
        self.secop_datatype = None


    @property
    def on_message(self):
        return self._on_message


    @on_message.setter
    def on_message(self, func):
        """ Setting the function called when a PV is updated. Function set in the LinkClient"""
        self._on_message = func


    def read_from_pv(self):
        """ Used if a PV read is requested. Normally read is handled by the subscribe setup below"""
        self.read_event_handler()


    def monitor(self):
        """Starts monitoring of the PV
        """
        if not self.subscribing:
            self.subscribe()
        self.pv_channel.startMonitor()
        self.monitoring = True
        print('Monitoring: ' + self.pv_name)


    def subscribe(self):
        """ A 'subscribe' function of the EpicsReader class object that initiates subscribe on the pvaccess channel.
        The 'read_event_handler' function is sent as arg and will be called when the PV is updated"""
        self.pv_channel.subscribe('read_event_handler', self.read_event_handler)
        self.subscribing = True


    def unsubscribe(self):
        self.pv_channel.unsubscribe('read_event_handler')
        self.subscribing = False


    def datatype(self):
        """Adjusting datatype to first recieved data from the connected PV.
        Returns the suitable secop type
            TODO: Investigaet is teh datatype should just be set from the link information! It shoudl be mandatory!
            but yes , it CAN be autodetected here! if that is useful... more a policy decision.
        """
        self.secop_datatype = datainfo.suggest(self.data)
        self_data_type = type(self.data)
        if not self.data:
            return None
        if self_data_type is float:
            self.secop_type = 'double'
            self.epics_type = pvaccess.DOUBLE
            return 'double'
        if self_data_type is int:
            self.secop_type = 'int'
            self.epics_type = pvaccess.LONG
            return 'int'
        #  TODO need to add handling of all types
        if self_data_type is dict:  #  Somewhat handling enums...
            if 'choices' in self.data.keys():
                self.secop_type = 'enum'
                ret = ["enum"]
                choices = {}
                a = 0
                for choice in self.data.get('choices'):
                    choices[choice] = a
                    a += 1
                ret.append(choices)
                return ret
        return None


    def read_event_handler(self, pv_object_data=None):
        """Triggered when a PV is updated (as setup with subscribed and monitor). The self.pv_channel PV is used
            and is setup in the class subscribe and monitor function. !!!! a parallel PV (ts_pv_channel) was used
            here for the timestamp but that has been exchanged for the pv_channel one as it should be redundant
            TODO: verify that the timestamp object (ts_pv_channel) is not required
            """
        if pv_object_data is None: #  TODO: Verify if thsi is redundant. Remove?
            retries = 3
            while retries > 0:
                try:
                    #   TODO think this is redundant...trying to fetch data if nothing recieved in callback..?
                    pv_object_data = self.pv_channel.get('')  #   TODO: Used to be ts_pv_channel here. Seem to work now. Is this OK?
                    retries = 0
                except pvaccess.PvaException as err:
                    retries -= 1
                    time.sleep(1)
                except Exception as err:
                    print('EpicsReader: error in reader_event_handler: ' + str(err))
                    retries = 0

        if not self.data:
            #  TODO: decide if this is useful. In principle this shoudl be known from user input (setting up links)
            try:
                self.unit = pv_object_data['display.units']
                self.description = pv_object_data['display.description']
                alarm = pv_object_data['alarm.message']
                timestamp_secs = pv_object_data['timeStamp.secondsPastEpoch']
            except pvaccess.FieldNotFound:
                #  TODO add handler
                pass
            self.data = pv_object_data['value']
            self.secop_type = self.datatype()
        else:
            self.data = pv_object_data['value']

        if self.on_message:  #  TODO: Not sure this is really needed?? Shoudl always have this..?
            tempdict = self.time_stamp_from_epics(pv_object_data)
            #  Adding the alarm value from the available PV alarm status (TODO: shoudl probably be the severity or status as int instead)
            tempdict["alarm"] = pv_object_data['alarm.message']
            self.on_message(self.pv_name, json.dumps([self.data, tempdict]))
        else:
            pass
            #print(json.dumps([self.data, self.time_stamp_from_epics(pv_object_data)]))


    def time_stamp_from_epics(self, pv_object):
        """Extracts the timestamp from the PV and returns as correctly formatted datarespons"""
        if type(pv_object) is pvaccess.PvObject:
            return {'t': pv_object['timeStamp.secondsPastEpoch'] + pv_object['timeStamp.nanoseconds'] * 1e-9}
        else:
            print('Not able to retrieve timestamp from EPICS PvObject, using time')
            return {'t': time.time()}

