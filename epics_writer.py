import time
import pvaccess


class EpicsWriter(object):
    def __init__(self, reference):
        self.reference = reference
        self._on_message = None
        self.pv_name = str(self.reference)
        self.pv_channel = pvaccess.Channel(self.pv_name)
        self.pvObject = None
        self.subscribing = False
        self.monitoring = False
        self.data = None
        self.secop_type = None
        self.epics_type = None
        self.unit = None
        self.description = None
        self.retries = 0

    def write_to_pv(self, value):
        """ Updating the a PV when triggered from a topic callback function.
        Need to be adjusted so different epics types are handled correctly
        TODO: new handling of enum and structures required
        """

        if self.epics_type == "enum":  # Sort out enums to write to the "value.index" on those..
            try:
                self.pv_channel.putInt(value, 'field(value.index)')
            except:
                print("Failed put to PV")

        else:  #  important: All other signals treated like this.. Shoudl probably be more detailed (putint for int etc..)
            try:
                self.pv_channel.put(value, 'field(value)')

            except pvaccess.PvaException:
                print('Failed to put to PV')

    def unsubscribe(self):
        self.pv_channel.unsubscribe('publish')
        self.subscribing = False
        print('Unsubscribing to: ' + self.pv_name)

    def datatype(self, x):
        #  TODO: Need to be updated to handle secop syntax
        if type(x) is dict:
            if "enum" in x.keys():
                self.epics_type = "enum"

            elif "int" in x.keys():
                self.epics_type = pvaccess.INT

            elif "double" in x.keys():
                self.epics_type = pvaccess.FLOAT

            elif "scaled" in x.keys():
                self.epics_type = enumerate

            elif "bool" in x.keys():
                self.epics_type = pvaccess.BOOLEAN

            elif "string" in x.keys():
                self.epics_type = pvaccess.STRING

            elif "blob" in x.keys():
                print("it is blob. need waweform...? not implemented")

            else:
                print(" This is not a implemented type...error")

        else:
            print("this is not a dict! Error")
            return None
        return None

    def time_stamp_from_epics(self, pv_object):
        """Extracts the timestamp from the PV and returns as correctly formatted datarespons"""
        if type(pv_object) is pvaccess.PvObject:
            return {'t': pv_object['timeStamp.secondsPastEpoch'] + pv_object['timeStamp.nanoseconds'] * 1e-9}
        else:
            print('Not able to retrieve timestamp from EPICS PvObject, using time')
            return {'t': time.time()}

