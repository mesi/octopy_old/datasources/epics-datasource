#!/usr/bin/python3
import paho.mqtt.client as mqtt
import logging
import json
import time
import sys
import argparse
import traceback
from os import path
from lib.octopyapp.octopyapp import OctopyApp
from epics_link import EpicsLink

APP_ID = 'EpicsDatasource'
LOG_LEVEL = logging.DEBUG

LOAD_TIMEOUT = 10
STATUS_UPDATE_INTERVAL = 5

LINK_STORAGE_CATEGORY = 'datasources'
LINK_STORAGE_KEY = 'epics'


class EpicsDatasource(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self._links = {}


    def start(self):
        super().start(start_loop = False)
        # Wait for MQTT connection
        logging.info("Waiting for MQTT connection")
        while not self.connected:
            self.mqtt_client.loop(0.1)
            self.mqtt_client.loop_misc()
        # Load links and wait for response
        self.links_loaded = False
        self.load_links()
        timeout = time.time() + LOAD_TIMEOUT
        while not self.links_loaded:
            logging.info(f"Waiting for links to be loaded from the Storage Manager")
            if time.time() > timeout:
                logging.warning(f"Loading links timeout. No response received from the Storage Manager.")
                break
            self.mqtt_client.loop(1)
            self.mqtt_client.loop_misc()
        self.subscribe(self.topics['datasource']['set channels'], self.on_mqtt_set_link_message)
        self.subscribe(self.topics['datasource']['remove channels'], self.on_mqtt_remove_link_message)
        tick = 0.1  # 10 Hz refresh rate
        last_status_update = time.time() - STATUS_UPDATE_INTERVAL
        last_mqtt_misc = time.time()
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                if current_time > last_status_update + STATUS_UPDATE_INTERVAL:
                    # Publish datasource status regularly
                    last_status_update = current_time
                    self.publish_status()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.mqtt_client.loop_misc()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        logging.debug('Exiting: ' + reason)
        for link_id, link in self._links.items():
            link.stop()
        self._links.empty()
        self.publish_status('removed')
        self.mqtt_client.loop()
        self.stop()
        sys.exit(0)


    def on_mqtt_load_links_message(self, client, userdata, msg):
        self.links_loaded = True
        logging.debug("Load link response received from Storage Manager")
        # Decode message
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"JSON encoding error for load links message: {err}")
            return
        # Check for errors from the storage manager and make sure we got a valid value
        if 'error' in payload:
            logging.warning(f"Could not load links from storage. Storage Manager says: {payload['error']}")
            return
        if not 'value' in payload:
            logging.warning(f"Can't load links: no value in response from Storage Manager.")
            return
        links_conf = payload['value']
        if not isinstance(links_conf, list):
            logging.warning(f"Can't load links: the value received from Storage Manager was not a list.")
            return
        # Create/update link
        try:
            self.set_links(links_conf)
        except Exception as err:
            logging.warning(f"Failed creating link list: {err}")
            traceback.print_exc()
        # Report success
        logging.info(f"Links loaded")
        logging.debug(f"Links: {list(self._links.keys())}")


    def on_mqtt_set_link_message(self, client, userdata, msg):
        logging.debug("Set link message")
        try:
            links_conf = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"Can't set link because of JSON encoding error: {err}")
            return
        if not isinstance(links_conf, list):
            links_conf = [links_conf]
        self.set_links(links_conf)
        self.store_links()


    def on_mqtt_remove_link_message(self, client, userdata, msg):
        logging.debug("Remove link message")
        # Verify that the message has the correct format
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"Can't remove link because of JSON encoding error: {err}")
            return
        if not isinstance(payload, list):
            logging.warning(f"Can't remove links: the message is not a list")
            return
        # Remove links, but only those belonging to this datasource.
        # The datasource id check is mostly a formality. As long as the topic isn't
        # shared between multiple gateways there should never be ambiguity here.
        for link_conf in payload:
            if not isinstance(link_conf, dict):
                logging.warning(f"Can't remove channel: the item is not a dict")
                continue
            datasource_id = link_conf.get('datasource id')
            if datasource_id != self.app_id:
                continue
            if not 'channel id' in link_conf:
                logging.warning(f"Can't remove channel: no channel-id given")
                continue
            link_id = link_conf['channel id']
            self.remove_link(link_id)


    def on_new_value_from_link(self, link, message):
        print(f"From PV: {message}")
        self.publish(link.value_topic, message)


    def on_get_request_from_mqtt(self, client, userdata, msg):
        for link in self._links.values():
            if link.get_topic ==  msg.topic:
                try:
                    link.on_mqtt_get_request()
                except BaseException as err:
                    logging.warning(f"Error on get request (\"{msg.topic}\"): {err}")


    def on_set_request_from_mqtt(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
            value = payload[0]
        except json.decoder.JSONDecodeError as err:
            logging.warning(f"JSON parse error when parsing value for set request (\"{msg.topic}\"): {err}")
        for link in self._links.values():
            if link.set_topic ==  msg.topic:
                try:
                    link.on_mqtt_set_request(value)
                except BaseException as err:
                    logging.warning(f"Error on set request (\"{msg.topic}\"): {err}")


    def publish_status(self, status = 'connected'):
        links = []
        for link in self._links.values():
            links.append(link.serialize())
        source_conf = {
            'datasource id': self.app_id,
            'type': 'epics',
            'status': status,
            't': time.time(),
            'links': links
        }
        payload = json.dumps(source_conf, indent = 4)
        topic = path.join(self.topics['datasource']['status'], self.app_id)
        self.publish(topic, payload)


    def remove_link(self, link_id):
        if not link_id in self._links:
            logging.warning("Can't remove link \"{link_id}\" because it doesn't exist in the datasource \"{self.app_id}\".")
            return
        link = self._links[link_id]
        logging.debug('Removing link: ' + str(link.serialize()))
        link.stop()
        del self._links[link_id]
        self.store_links()
        self.publish_status()


    def set_links(self, conf):
        # Filter out only links destined for this gateway
        for link_conf in conf:
            if isinstance(link_conf, dict) and link_conf.get('datasource id') == self.app_id:
                link_id = link_conf.get('value topic')
                logging.debug(f"Set link: {link_id}")
                link = None
                if link_id in self._links:
                    # Link already exists, update it
                    link = self._links[link_id]
                    link.deserialize(link_conf)
                else:
                    # New link
                    link = EpicsLink(link_conf)
                    self._links[link_id] = link
                self.setup_link_data_flow(link)
        self.publish_status()


    def setup_link_data_flow(self, link):
        logging.debug(f"Setup data flow for link: {link.channel_id}")
        if link.get_reference and link.value_topic:
            # Setup callback for data received from EPICS
            link.on_new_value = self.on_new_value_from_link
        if link.get_topic:
            # Setup MQTT message callbacks for get requests
            self.subscribe(link.get_topic, self.on_get_request_from_mqtt)
        if link.set_reference and link.set_topic:
            # Setup MQTT message callbacks for set requests
            self.subscribe(link.set_topic, self.on_set_request_from_mqtt)


    def store_links(self):
        """ Store links in storage manager"""
        logging.debug('Storing links to storage manager')
        links = []
        try:
            for link in self._links.values():
                links.append(link.serialize())
        except TypeError as err:
            logging.error('TcpLinkClient.store_links: {}'.format(err))
            return
        logging.info('Storing links on {}: {}'.format(self.topics['storage']['set'], json.dumps(links, indent=4)))
        payload = {
            'value': links,
            'category': LINK_STORAGE_CATEGORY,
            'key': LINK_STORAGE_KEY
        }
        self.publish(topic = self.topics['storage']['set'], payload = json.dumps(payload))


    def load_links(self):
        logging.info("Loading links")
        self.subscribe(self.topics['epics datasource']['load links'], self.on_mqtt_load_links_message)
        payload = {
            'return topic': self.topics['epics datasource']['load links'],
            'category': LINK_STORAGE_CATEGORY,
            'key': LINK_STORAGE_KEY
        }
        self.publish(topic = self.topics['storage']['get'], payload = json.dumps(payload))


    def add_link_fnk(self, lnk):
        logging.error('implement a add_link_fnk in child class to handle adding of links')


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {},
        'storage': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    eds = EpicsDatasource(config)
    eds.start()
